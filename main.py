import webapp2
from google.appengine.api import memcache
from google.appengine.api import urlfetch

# TODO: cache results. Leaving example code here for now.
def put_memcache():
  memcache.add(key="weather_USA_98105", value="raining", time=3600)
  memcache.get(key="blah")

class VideoHandler(webapp2.RequestHandler):
  def get_base(self):
    raise NotImplementedError("get_base")

  def get(self):
    """Handle GET requests."""
    # TODO: application/json
    self.response.content_type = 'text/plain'
    result = urlfetch.fetch(self.get_base() + self.request.query_string)
    if result.status_code == 200:
      self.response.write(result.content)
    else:
      self.response.write('failure')

class YoutubeHandler(VideoHandler):
  def get_base(self):
    return "http://www.youtube.com/get_video_info?"

class DriveVideoHandler(VideoHandler):
  def get_base(self):
    return "https://docs.google.com/get_video_info?"

APP = webapp2.WSGIApplication([
    ('/get_youtube_info', YoutubeHandler),
    ('/get_drive_video_info', DriveVideoHandler),
], debug=True)

