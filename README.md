Video "randomizer" player for Milanova.
==============

# Prerequisites

Download the [Closure compiler](http://dl.google.com/closure-compiler/compiler-latest.zip) and extract it into the "closure-compiler" directory.

    # Download this project
    git clone https://bitbucket.org/maharajah/milovana-randomizer.git
    cd milovana-randomizer
    # Download the closure library into "closure-library"
    git clone https://github.com/google/closure-library
    # Download and unzip the closure compiler into "closure-compiler"
    tmp=`mktemp` && wget -O $tmp http://dl.google.com/closure-compiler/compiler-latest.zip && mkdir closure-compiler && (cd closure-compiler && unzip $tmp)
    # Download the Google Appengine library and extract to "google_appengine"
    https://developers.google.com/appengine/downloads#Google_App_Engine_SDK_for_Python
    unzip google_appengine*.zip -d google_appengine

# Building

    # Generate a release version
    make clean && make release
    cat bin/demo.html

    # Generate a debug version
    make clean && make debug
    cat bin/demo-debug.html

    # Generate both
    make clean && make all

# Running locally

    ./google_appengine/dev_appserver.py debug.yaml
    # Open browser to http://localhost:8080

# Embedding

    <script src="compiled.js"></script>
    <div id="player"></div>
    <script>
    vr.VideoRandomizer.initialize(
        document.getElementById('player'),
        '/database.xml');
    </script>

That's it!

# Tech Overview

JS is compiled using Closure.
XML database format remains the same as always, with a new VideoId blurb for Google Video ids.
