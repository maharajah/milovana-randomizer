goog.provide('vr.UrlSyncer');

goog.require('vr.VideoPlaylist');



/**
 * Handles URL arguments for different settings, to allow users to share links
 * to video playlists/settings/etc.
 * @constructor
 * @struct
 */
vr.UrlSyncer = function() {
};


/**
 * Gets the playlist from the current URL, or null if not present.
 * @param {!vr.VideoDatabase} database
 * @return {vr.VideoPlaylist}
 */
vr.UrlSyncer.prototype.getPlaylist = function(database) {
  // TODO: not really important for first iteration.
  return null;
};
