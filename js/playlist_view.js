goog.provide('vr.PlaylistView');

goog.require('goog.dom');
goog.require('goog.dom.classlist');
goog.require('goog.dom.dataset');
goog.require('goog.fx.DragListGroup');
goog.require('goog.ui.Component');



/**
 * UI for manipulating the current playlist.
 * 
 * @param {!vr.VideoPlaylist} playlist
 * @constructor
 * @struct
 * @extends {goog.ui.Component}
 */
vr.PlaylistView = function(playlist) {
  goog.ui.Component.call(this);

  /** @private {!vr.VideoPlaylist} */
  this.playlist_ = playlist;
};
goog.inherits(vr.PlaylistView, goog.ui.Component);


/** @override */
vr.PlaylistView.prototype.createDom = function() {
  vr.PlaylistView.base(this, 'createDom');
  goog.dom.classlist.set(this.getElement(), 'playlist-view');
};


/** @override */
vr.PlaylistView.prototype.enterDocument = function() {
  vr.PlaylistView.base(this, 'enterDocument');

  this.getHandler().
      listen(this.playlist_, vr.VideoPlaylist.EventType.CHANGE,
          this.refreshView_);

  // Refresh after entering
  this.refreshView_();
};


/** @private */
vr.PlaylistView.prototype.refreshView_ = function() {
  this.getContentElement().innerHTML = '';

  var currentRound = this.playlist_.getCurrentRound();
  this.playlist_.getAllRounds().forEach(function(round) {
    var roundDiv = goog.dom.createDom('div',
        'playlist-view-round', [round.video.name, round.name].join(' - '));
    if (round == currentRound) {
      goog.dom.classlist.add(roundDiv, 'playlist-view-round-current');
    }

    // TODO round ids

    this.getContentElement().appendChild(roundDiv);
  }, this);
};
