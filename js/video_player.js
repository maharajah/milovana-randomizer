goog.provide('vr.VideoPlayer');

// Special group: things Closure doesn't include correctly.
goog.require('goog.debug.ErrorHandler');
goog.require('goog.events.EventWrapper');
goog.require('goog.Uri');

goog.require('goog.asserts');
goog.require('goog.dom');
goog.require('goog.dom.classes');
goog.require('goog.events.EventType');
goog.require('goog.ui.Component');
goog.require('vr.PlaylistEditor');
goog.require('vr.media.MediaEventType');
goog.require('vr.media.MediaPlayerFactory');



/**
 * Generalized video player to manage a single playlist.
 * Delegates media handling to individual media players.
 * @constructor
 * @struct
 * @extends {goog.ui.Component}
 */
vr.VideoPlayer = function() {
  goog.ui.Component.call(this);

  /** @private {vr.VideoPlaylist} */
  this.playlist_ = null;

  /** @private {?vr.Round} */
  this.currentRound_ = null;

  /** @private {vr.VideoPlayerState} */
  this.state_ = vr.VideoPlayerState.STOPPED;

  /** @private {vr.media.MediaPlayer} */
  this.currentPlayer_ = null;

  /** @private {!Element} */
  this.playerElement_ = goog.dom.createDom('div',
      'video-player-media-container');

  /** @private {!Element} */
  this.nextControl_ = goog.dom.createDom('button',
      'video-player-control video-player-control-next', '>|');

  /** @private {!Element} */
  this.previousControl_ = goog.dom.createDom('button',
      'video-player-control video-player-control-previous', '|<');

  /** @private {!Element} */
  this.statusElement_ = goog.dom.createDom('div', 'video-status');
};
goog.inherits(vr.VideoPlayer, goog.ui.Component);


/** @enum {number} */
vr.VideoPlayerState = {
  STOPPED: 0,
  PAUSED: 1,
  PLAYING: 2
};


/**
 * Sets the given playlist as the current playlist, stopping playback if
 * necessary.
 * Does not begin playing the new playlist.
 * @param {vr.VideoPlaylist} playlist
 */
vr.VideoPlayer.prototype.loadPlaylist = function(playlist) {
  if (this.state_ == vr.VideoPlayerState.PLAYING) {
    // TODO stop playback
    this.state_ = vr.VideoPlayerState.STOPPED;
  }

  this.playlist_ = playlist;
};


/** @override */
vr.VideoPlayer.prototype.createDom = function() {
  vr.VideoPlayer.base(this, 'createDom');

  goog.dom.classes.add(this.getContentElement(), 'video-player');

  var element = goog.asserts.assertElement(this.getContentElement());

  // 1) Render the player element
  element.appendChild(this.playerElement_);

  // 2) Render the status element and video controls
  // Note: for CSS reasons, controls need to be direct children of content
  // element, otherwise we need a control container that covers up the media
  // element (avoidable if possible).
  goog.dom.append(element,
      this.previousControl_, this.nextControl_, this.statusElement_);

  // 3) Render the "edit playlist" control
  goog.asserts.assert(this.playlist_, 'Must have a playlist');
  var playlistEditor = new vr.PlaylistEditor(this.playlist_);
  this.addChild(playlistEditor, true);
};


/** @override */
vr.VideoPlayer.prototype.enterDocument = function() {
  vr.VideoPlayer.base(this, 'enterDocument');

  this.getHandler().
      listen(this.nextControl_, goog.events.EventType.CLICK,
          this.advanceRound_).
      listen(this.previousControl_, goog.events.EventType.CLICK,
          this.retreatRound_).
      listen(this.playlist_, vr.VideoPlaylist.EventType.CHANGE,
          this.onRoundChanged_);
};


/**
 * Loads the current round, creates a new player and plays the round.
 * @private
 */
vr.VideoPlayer.prototype.reloadAndPlay_ = function() {
  this.currentRound_ = this.playlist_.getCurrentRound();

  // End of playlist.
  // TODO set state to stopped
  if (this.currentRound_ == null) return;

  // TODO Check if we have a player already for this round. Recreating means
  // we have to restart the round, and can't actually pause/resume.
  this.rebuildPlayer_();
  this.play_();
};


/**
 * Builds a new MediaPlayer instance for the current round.
 * @private
 */
vr.VideoPlayer.prototype.rebuildPlayer_ = function() {
  if (this.currentRound_ == null) return;

  var newPlayer = vr.media.MediaPlayerFactory.getPlayer(this.currentRound_);
  newPlayer.loadRound(this.currentRound_);

  // Destroy and re-render player if different
  if (newPlayer != this.currentPlayer_) {
    goog.dispose(this.currentPlayer_);
    newPlayer.render(this.playerElement_);
    this.getHandler().listen(newPlayer,
        vr.media.MediaEventType.FINISHED, this.advanceRound_);
    this.currentPlayer_ = newPlayer;
  }
};


/**
 * Plays the current round.
 * @private
 */
vr.VideoPlayer.prototype.play_ = function() {
  this.state_ = vr.VideoPlayerState.PLAYING;
  this.currentPlayer_.resume();
  this.updateStatus_();
};


/** @private */
vr.VideoPlayer.prototype.updateStatus_ = function() {
  var lengthSeconds = this.currentRound_.endTimeSeconds -
      this.currentRound_.startTimeSeconds;
  var displaySeconds = lengthSeconds % 60;
  var displayMinutes = (lengthSeconds - displaySeconds) / 60;
  if (displaySeconds < 10) {
    displaySeconds = '0' + displaySeconds;
  }

  this.statusElement_.textContent = [
    this.currentRound_.video.name,
    this.currentRound_.name,
    '(' + displayMinutes + ':' + displaySeconds + ')'
  ].join(' - ');
};


/**
 * Immediately moving on to the next round.
 * @private
 */
vr.VideoPlayer.prototype.advanceRound_ = function() {
  if (!this.playlist_.hasNextRound()) return;

  // Ensure current playback is stopped
  this.currentPlayer_.pause();
  this.playlist_.changeNextRound();
};


/**
 * Moves to the previous round, if possible.
 * @private
 */
vr.VideoPlayer.prototype.retreatRound_ = function() {
  if (!this.playlist_.hasPreviousRound()) return;

  // Ensure current playback is stopped
  this.currentPlayer_.pause();
  this.playlist_.changePreviousRound();
};


/** @private */
vr.VideoPlayer.prototype.onRoundChanged_ = function() {
  // Handles everything!
  this.reloadAndPlay_();
};


/**
 * Pauses the current playback.
 * @private
 */
vr.VideoPlayer.prototype.pause_ = function() {
  this.state_ = vr.VideoPlayerState.PAUSED;
  this.currentPlayer_.pause();
};
