goog.provide('vr');
goog.provide('vr.VideoRandomizer');

goog.require('vr.VideoPlayer');
goog.require('vr.UrlSyncer');
goog.require('vr.XmlLoader');



/**
 * This is why you're all here, right?
 *
 * @param {!Element} container Element to render the entire player into.
 * @param {string} databasePath Path to the XML database.
 * @constructor
 * @struct
 */
vr.VideoRandomizer = function(container, databasePath) {
  /** @private {!vr.XmlLoader} */
  this.loader_ = new vr.XmlLoader(databasePath);

  /** @private {!vr.UrlSyncer} */
  this.urlSyncer_ = new vr.UrlSyncer();

  /** @private {vr.VideoDatabase} */
  this.videoDatabase_ = null;

  /** @private {!vr.VideoPlayer} */
  this.player_ = new vr.VideoPlayer();

  /** @private {!Element} */
  this.container_ = container;
};


/**
 * Main entry point.
 * @param {!Element} container Where to render the randomizer.
 * @param {string} databasePath Path to the XML database.
 * @export
 */
vr.VideoRandomizer.initialize = function(container, databasePath) {
  var randomizer = new vr.VideoRandomizer(container, databasePath);
  randomizer.initialize_();
  return window['randomizer'] = randomizer;
};


/** @private */
vr.VideoRandomizer.prototype.initialize_ = function() {
  this.loader_.load(this.onDataLoaded_, this);
};


/**
 * @param {!vr.VideoDatabase} database
 * @private
 *
 * TODO rm
 * @suppress {accessControls}
 */
vr.VideoRandomizer.prototype.onDataLoaded_ = function(database) {
  this.videoDatabase_ = database;

  // Get playlist from UrlSyncer, if present in the URL
  var playlist = this.urlSyncer_.getPlaylist(database) ||
      this.videoDatabase_.generateDefaultPlaylist();
  this.player_.loadPlaylist(playlist);

  // 3) Render the player
  this.player_.render(this.container_);

  // TODO move this to a user UI action so it's not immediate.
  // 4) Play
  this.player_.reloadAndPlay_();
};

