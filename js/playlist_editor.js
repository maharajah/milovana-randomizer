goog.provide('vr.PlaylistEditor');

goog.require('goog.dom');
goog.require('goog.style');
goog.require('goog.ui.Component');
goog.require('goog.ui.Popup');
goog.require('vr.PlaylistView');



/**
 * Contains the entire playlist editor UI. Renders itself primarily as the
 * show/hide button.
 * @param {!vr.VideoPlaylist} playlist
 * @constructor
 * @struct
 * @extends {goog.ui.Component}
 */
vr.PlaylistEditor = function(playlist) {
  goog.ui.Component.call(this);

  /** @private {!vr.VideoPlaylist} */
  this.playlist_ = playlist;

  /** @private {!vr.PlaylistView} */
  this.playlistView_ = new vr.PlaylistView(this.playlist_);

  /** @private {!Element} */
  this.button_ = goog.dom.createDom('button', 'playlist-editor-open',
      'Edit Playlist');

  /** @private {!Element} */
  this.popupEl_ = goog.dom.createDom('div', 'playlist-editor-popup');
  goog.style.setElementShown(this.popupEl_, false);

  // TODO positioning
  /** @private {!goog.ui.Popup} */
  this.popup_ = new goog.ui.Popup(this.popupEl_);
  this.popup_.setHideOnEscape(true);
};
goog.inherits(vr.PlaylistEditor, goog.ui.Component);


/** @override */
vr.PlaylistEditor.prototype.createDom = function() {
  vr.PlaylistEditor.base(this, 'createDom');

  this.getContentElement().appendChild(this.button_);

  // Popup needs to be in page at some point, even though hidden.
  document.body.appendChild(this.popupEl_);

  // Build popup contents
  this.popupEl_.appendChild(goog.dom.createDom('h3', 'playlist-editor-title',
        'Edit Playlist (in progress)'));

  // Render PlaylistView into popup.
  this.addChild(this.playlistView_, true);
  this.popupEl_.appendChild(this.playlistView_.getElement());
};


/** @override */
vr.PlaylistEditor.prototype.enterDocument = function() {
  vr.PlaylistEditor.base(this, 'enterDocument');

  this.getHandler().listen(
      this.button_, goog.events.EventType.CLICK, this.openPopup_);
};


/** @private */
vr.PlaylistEditor.prototype.openPopup_ = function() {
  this.popup_.setVisible(true);
};
