goog.provide('vr.media.MediaPlayerFactory');

goog.require('vr.media.GoogleDrivePlayer');
goog.require('vr.media.GoogleDriveHtml5Player');
goog.require('vr.VideoSource');


/** @private {vr.media.MediaPlayer} */
vr.media.MediaPlayerFactory.currentPlayerInstance_ = null;


/**
 * Yes, I realize this is potentially a bit overkill currently.
 * However, it provides a clean separation between the source-abstract
 * VideoPlayer and potentially different video players that we may have.
 * @param {!vr.Round} round Round for which to get a player.
 * @return {!vr.media.MediaPlayer}
 */
vr.media.MediaPlayerFactory.getPlayer = function(round) {
  var playerCtor = null;

  // TODO: test if HTML5 video is supported?
  // Maybe via Settings object that we pass in to enable this feature
  // intentionally.
  if (round.video.videoSource == vr.VideoSource.GOOGLE_DRIVE) {
    playerCtor = vr.media.GoogleDriveHtml5Player;
  } else if (round.video.videoSource == vr.VideoSource.GOOGLE_DRIVE) {
    playerCtor = vr.media.GoogleDrivePlayer;
  }

  if (!playerCtor) {
    throw new Error('Could not find player for video');
  }

  // Reuse existing player if possible. This prevents destroying fullscreen
  // state, volume, etc. and generally results in smoother playback.
  var currentPlayer = vr.media.MediaPlayerFactory.currentPlayerInstance_;
  if (currentPlayer && currentPlayer instanceof playerCtor) {
    console.log('reusing player');
    return currentPlayer;
  }

  var newPlayer = new playerCtor(round);
  vr.media.MediaPlayerFactory.currentPlayerInstance_ = newPlayer;
  return newPlayer;
};
