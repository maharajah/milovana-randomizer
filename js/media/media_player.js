goog.provide('vr.media.MediaPlayer');
goog.provide('vr.media.MediaEventType');

goog.require('goog.ui.Component');



/**
 * Abstract media player class.
 * @constructor
 * @struct
 * @extends {goog.ui.Component}
 */
vr.media.MediaPlayer = function() {
  goog.ui.Component.call(this);

  /** @private {?vr.Round} */
  this.currentRound_ = null;

  /** @private {boolean} */
  this.isPlaying_ = false;
}
goog.inherits(vr.media.MediaPlayer, goog.ui.Component);


/** @enum {string} */
vr.media.MediaEventType = {
  FINISHED: 'FINISHED',
  PLAY: 'PLAY',
};


/**
 * Loads a given round from the current video.
 * Must be able to be called multiple times in order to load a new round.
 * @param {!vr.Round} round
 */
vr.media.MediaPlayer.prototype.loadRound = function(round) {
  // Pause, in case already playing.
  this.pause();

  this.currentRound_ = round;
}


/**
 * @return {?vr.Round} Currently playing round.
 */
vr.media.MediaPlayer.prototype.getCurrentRound = function() {
  return this.currentRound_;
};


/**
 * Pause the current media player, if possible.
 */
vr.media.MediaPlayer.prototype.pause = function() {
  this.isPlaying_ = false;
};


/**
 * Resume playback in the current media player, if possible.
 */
vr.media.MediaPlayer.prototype.resume = function() {
  this.isPlaying_ = true;
};


/**
 * "Is playing" state is a little tricky. This should be "as known by the
 * application", not necessarily "is the video currently being shown to the
 * user." Sometimes there are delays between the two, and the two will 100%
 * not be in sync until controls are externalized from the players.
 * @return {boolean}
 */
vr.media.MediaPlayer.prototype.isPlaying = function() {
  return this.isPlaying_;
};
