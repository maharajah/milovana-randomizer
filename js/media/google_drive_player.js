goog.provide('vr.media.GoogleDrivePlayer');

goog.require('goog.dom');
goog.require('goog.style');
goog.require('vr.media.MediaPlayer');
goog.require('vr.VideoSource');



/**
 * @param {!vr.Round} round
 * @constructor
 * @struct
 * @extends {vr.media.MediaPlayer}
 */
vr.media.GoogleDrivePlayer = function(round) {
  vr.media.MediaPlayer.call(this);

  if (round.video.videoSource != vr.VideoSource.GOOGLE_DRIVE) {
    throw new Error('Wrong video source for GoogleDrivePlayer');
  }

  /** @private {number} */
  this.timer_ = 0;

  /** @private {!Element} */
  this.playerElement_ = this.buildPlayer_();
};
goog.inherits(vr.media.GoogleDrivePlayer, vr.media.MediaPlayer);


/** @override */
vr.media.GoogleDrivePlayer.prototype.createDom = function() {
  vr.media.GoogleDrivePlayer.base(this, 'createDom');
  this.getElement().appendChild(this.playerElement_);
};


/** @override */
vr.media.GoogleDrivePlayer.prototype.enterDocument = function() {
  vr.media.GoogleDrivePlayer.base(this, 'enterDocument');

  // Simplification: assume player has width:100% on the page.
  var playerWidth = window.innerWidth;
  // Ideal player ratio is roughly 1.7.
  var idealPlayerHeight = Math.round(playerWidth / 1.7);
  // Leave room for the controls
  var maxPlayerHeight = window.innerHeight - 200;
  var playerHeight = Math.min(maxPlayerHeight, idealPlayerHeight);

  goog.style.setHeight(this.playerElement_, playerHeight);
};


/**
 * @return {!Element}
 * @private
 */
vr.media.GoogleDrivePlayer.prototype.buildPlayer_ = function() {
  var element = goog.dom.createDom('div');

  // TODO Soy or something, this is awful
  element.innerHTML =
      '<object type="application/x-shockwave-flash" ' +
          'class="google-drive-player"' +
          'allowscriptaccess="always" allowfullscreen="true" wmode="opaque" ' +
          'id="vpl0" style="visibility: visible;">' +
        '<param name="allowFullScreen" value="true">' +
        '<param name="allowscriptaccess" value="always">' +
        '<param name="wmode" value="opaque">' +
      '</object>';

  // Lol, non-nullability.
  var object = element.firstElementChild;
  if (!object) throw new Error('Should never happen.');
  return object;
};


/** @override */
vr.media.GoogleDrivePlayer.prototype.loadRound = function(round) {
  vr.media.GoogleDrivePlayer.base(this, 'loadRound', round);

  // Wait for YT to be ready, then play
  window['onYouTubePlayerReady'] = this.onPlayerLoaded_.bind(this);

  this.playerElement_.setAttribute('data', this.generateUrl_(round));
  window.console.log(this.generateUrl_(round));
};


/**
 * Generates the video object URL for a given round.
 * @param {!vr.Round} round
 * @return {string}
 * @private
 */
vr.media.GoogleDrivePlayer.prototype.generateUrl_ = function(round) {
  return 'https://video.google.com/get_player?' +
      [
        'enablejsapi=1',
        'docid=' + round.video.videoIdentifier,
        'ps=docs',
        'partnerid=30',
        'cc_load_policy=1'
      ].join('&');
};


/** @private */
vr.media.GoogleDrivePlayer.prototype.onPlayerLoaded_ = function() {
  // YT player only accepts event listener functions by global string names.
  // #ThanksObama
  var callbackName = 'callback' + Math.round(Math.random() * 1000);
  window[callbackName] = this.onPlayerStateChange_.bind(this);
  if (this.playerElement_['addEventListener']) {
    this.playerElement_['addEventListener'].call(
        this.playerElement_, 'onStateChange', callbackName);
  }

  // seekTo starting position should automatically begin playback
  if (this.playerElement_['seekTo']) {
      this.playerElement_['seekTo'].call(
          this.playerElement_, this.getCurrentRound().startTimeSeconds);
  }
};


/** @private */
vr.media.GoogleDrivePlayer.prototype.onPlayerStateChange_ = function(state) {
  // Not playing
  if (this.timer_ && state != 1) {
    this.clearTimer_();
  }

  // Playing, either at the start or if the user adjusted the location
  if (state == 1) {
    var endTimeSeconds = this.getCurrentRound().endTimeSeconds;
    var currentTimeSeconds = this.playerElement_['getCurrentTime'] ?
        this.playerElement_['getCurrentTime'].call(this.playerElement_) :
        // Fallback to start time if we can't call "getCurrentTime"
        this.getCurrentRound().startTimeSeconds;

    this.clearTimer_();

    // Dispatch FINISHED event after time has elapsed to end of round, or
    // if the round is already over (timeRemainingSeconds == 0)
    var timeRemainingSeconds = Math.max(endTimeSeconds - currentTimeSeconds, 0);
    window.console.log('Time remaining: ' + timeRemainingSeconds);
    this.timer_ = window.setTimeout(
        this.dispatchEvent.bind(this, vr.media.MediaEventType.FINISHED),
        timeRemainingSeconds * 1000);
  }

  // Ended
  if (state == 0) {
    this.dispatchEvent(vr.media.MediaEventType.FINISHED);
  }
};


/** @override */
vr.media.GoogleDrivePlayer.prototype.pause = function() {
  vr.media.GoogleDrivePlayer.base(this, 'pause');
  if (this.playerElement_['pauseVideo']) {
    this.playerElement_['pauseVideo'].call(this.playerElement_);
  }
};


/** @override */
vr.media.GoogleDrivePlayer.prototype.resume = function() {
  vr.media.GoogleDrivePlayer.base(this, 'resume');
  if (this.playerElement_['playVideo']) {
    this.playerElement_['playVideo'].call(this.playerElement_);
  }
};


/** @override */
vr.media.GoogleDrivePlayer.prototype.disposeInternal = function() {
  vr.media.GoogleDrivePlayer.base(this, 'disposeInternal');
  this.clearTimer_();
};


/** @private */
vr.media.GoogleDrivePlayer.prototype.clearTimer_ = function() {
  if (!this.timer_) return;

  window.clearTimeout(this.timer_);
  this.timer_ = 0;
};
