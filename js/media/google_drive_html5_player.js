goog.provide('vr.media.GoogleDriveHtml5Player');

goog.require('goog.dom');
goog.require('goog.net.XhrIo');
goog.require('goog.Uri.QueryData');
goog.require('vr.media.MediaPlayer');
goog.require('vr.VideoSource');



/**
 * Plays a Google Drive video in an HTML5 player.
 * Note: this should be able to be easily generalized to handle YouTube videos,
 * though arguably YouTube's player has better HTML5 support to begin with.
 * @param {!vr.Round} round
 * @constructor
 * @struct
 * @extends {vr.media.MediaPlayer}
 */
vr.media.GoogleDriveHtml5Player = function(round) {
  vr.media.MediaPlayer.call(this);

  if (round.video.videoSource != vr.VideoSource.GOOGLE_DRIVE) {
    throw new Error('Wrong video source for GoogleDriveHtml5Player');
  }

  /** @private {!Element} */
  this.playerElement_ = goog.dom.createDom('video', {
    'class': 'html5-player',
    // TODO: consider a setting to disable controls
    'controls': true,
    'preload': 'metadata',
  });
};
goog.inherits(vr.media.GoogleDriveHtml5Player, vr.media.MediaPlayer);


/** @override */
vr.media.GoogleDriveHtml5Player.prototype.createDom = function() {
  vr.media.GoogleDriveHtml5Player.base(this, 'createDom');
  this.getElement().appendChild(this.playerElement_);
};


/** @override */
vr.media.GoogleDriveHtml5Player.prototype.enterDocument = function() {
  vr.media.GoogleDriveHtml5Player.base(this, 'enterDocument');

  this.getHandler().
      listen(this.playerElement_, 'loadedmetadata', this.onLoadedMetadata_).
      // Handle timeupdate and ended events similarly
      listen(this.playerElement_, ['timeupdate', 'ended'], this.onTimeUpdate_);
};


/** @override */
vr.media.GoogleDriveHtml5Player.prototype.loadRound = function(round) {
  vr.media.GoogleDriveHtml5Player.base(this, 'loadRound', round);

  goog.net.XhrIo.send(
      '/get_drive_video_info?docid=' + round.video.videoIdentifier,
      this.onVideoInfoLoaded_.bind(this));
};


/** @private */
vr.media.GoogleDriveHtml5Player.prototype.onVideoInfoLoaded_ = function(e) {
  var response = e.target.getResponseText();
  var queryData = new goog.Uri.QueryData(e.target.getResponseText());
  var status = queryData.getValues('status')[0];

  // TODO: error messaging. Probably return event.FAILED
  if (status != 'ok') {
    this.dispatchEvent(vr.media.MediaEventType.FINISHED);
    return;
  }

  var fmtList = this.buildFmtList_(queryData.getValues('fmt_list')[0]);
  var fmtStreamMap =
      this.buildFmtStreamMap_(queryData.getValues('fmt_stream_map')[0]);

  var selectedFormat = this.selectBestFormat_(fmtList);
  var videoUrl = fmtStreamMap[selectedFormat.itag];
  this.playerElement_.setAttribute('src', videoUrl);
  window.console.log(videoUrl);
};


/**
 * Parses the "fmt_list" info value.
 * TODO: enum type this return value
 * @param {string} fmtListStr
 * @return {!Array}
 * @private
 */
vr.media.GoogleDriveHtml5Player.prototype.buildFmtList_ = function(fmtListStr) {
  return fmtListStr.split(',').map(function(fmtString) {
    // What a bizarre structure. Comma-separated "format groups", where each
    // group is four values separated by a slash. Not sure what all of these
    // do yet.
    var splitString = fmtString.split('/');
    return {
      itag: splitString[0],
      resolution: splitString[1],
      unknown1: splitString[2],
      unknown2: splitString[3],
      unknown3: splitString[4],
    };
  });
};


/**
 * Parses the "fmt_stream_map" info value.
 * @param {string} fmtMapStr
 * @return {!Object.<string, string>} Map of itag to url.
 * @private
 */
vr.media.GoogleDriveHtml5Player.prototype.buildFmtStreamMap_ =
    function(fmtMapStr) {
  var map = {};
  fmtMapStr.split(',').forEach(function(fmtString) {
    var splitString = fmtString.split('|');
    map[splitString[0]] = splitString[1];
  });
  return map;
};


/**
 * @param {!Array} fmtList
 * @private
 */
vr.media.GoogleDriveHtml5Player.prototype.selectBestFormat_ =
    function(fmtList) {
  // TODO: intelligent format selection. Try to select among the best
  // resolutions and/or what the user can play.
  var mp4Itags = [160, 137, 136, 135, 134, 133, 85, 84, 83, 82, 38, 37, 22, 18];
  var webMItags = [248, 247, 244, 243, 242, 102, 101, 100, 46, 45, 44, 43];

  for (var i = 0, mp4ITag; mp4ITag = mp4Itags[i]; i++) {
    for (var j = 0, fmt; fmt = fmtList[j]; j++) {
      if (fmt.itag == mp4ITag) {
        window.console.log('Selecting MP4 format: ', fmt);
        return fmt;
      }
    }
  }

  for (var i = 0, webMItag; webMItag = webMItags[i]; i++) {
    for (var j = 0, fmt; fmt = fmtList[j]; j++) {
      if (fmt.itag == webMItag) {
        window.console.log('Selecting WebM format: ', fmt);
        return fmt;
      }
    }
  }

  window.console.log('Selecting other format: ', fmtList[0]);
  return fmtList[0];
};


/** @private */
vr.media.GoogleDriveHtml5Player.prototype.onLoadedMetadata_ = function() {
  this.playerElement_.currentTime = this.getCurrentRound().startTimeSeconds;
  this.playerElement_.play();
};


/** @private */
vr.media.GoogleDriveHtml5Player.prototype.onTimeUpdate_ = function() {
  if ((this.playerElement_.currentTime >=
      this.getCurrentRound().endTimeSeconds && this.isPlaying()) ||
      this.playerElement_.ended) {
    this.dispatchEvent(vr.media.MediaEventType.FINISHED);
  }
};


/** @override */
vr.media.GoogleDriveHtml5Player.prototype.pause = function() {
  vr.media.GoogleDriveHtml5Player.base(this, 'pause');
  this.playerElement_.pause();
};


/** @override */
vr.media.GoogleDriveHtml5Player.prototype.resume = function() {
  vr.media.GoogleDriveHtml5Player.base(this, 'resume');
  this.playerElement_.play();
};
