goog.provide('vr.VideoDatabase');

goog.require('goog.array');
goog.require('vr.VideoPlaylist');



/**
 * @param {!Array.<!vr.Video>} videos
 * @constructor
 * @struct
 */
vr.VideoDatabase = function(videos) {
  /** @private {!Array.<!vr.Video>} */
  this.videos_ = videos;
};


/**
 * Automatically builds a simple playlist from the contained videos.
 * @return {!vr.VideoPlaylist}
 */
vr.VideoDatabase.prototype.generateDefaultPlaylist = function() {  
  var allRounds = [];
  this.videos_.forEach(function(video) {
    goog.array.extend(allRounds, video.rounds);
  });
  goog.array.shuffle(allRounds);
  return new vr.VideoPlaylist(allRounds);
};
