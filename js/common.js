goog.provide('vr.Round');
goog.provide('vr.Video');
goog.provide('vr.VideoSource');


/**
 * @typedef {{
 *   endTimeSeconds: number,
 *   startTimeSeconds: number,
 *   name: string,
 *   video: vr.Video
 * }}
 */
vr.Round;


/** @enum {string} */
vr.VideoSource = {
  GOOGLE_DRIVE: 'GOOGLE_DRIVE'
};


/**
 * @typedef {{
 *   name: string,
 *   rounds: !Array.<!vr.Round>,
 *   videoIdentifier: string,
 *   videoSource: vr.VideoSource
 * }}
 */
vr.Video;
