goog.provide('vr.VideoPlaylist');
goog.provide('vr.VideoPlaylist.EventType');

goog.require('goog.events.EventTarget');



/**
 * Represents a playlist with a set of rounds to be played.
 * This class is a model of the current playlist and expected to be modified
 * by PlaylistManager.
 * 
 * @param {!Array.<!vr.Round>} rounds
 * @param {vr.VideoPlaylist.Settings=} opt_settings
 * @constructor
 * @struct
 * @extends {goog.events.EventTarget}
 */
vr.VideoPlaylist = function(rounds, opt_settings) {
  goog.events.EventTarget.call(this);

  // TODO: split into defaults
  /** @private {!vr.VideoPlaylist.Settings} */
  this.settings_ = opt_settings || {repeat: false};

  /** @private {!Array.<!vr.Round>} */
  this.rounds_ = rounds;

  /** @private {number} */
  this.currentRoundIndex_ = 0;
};
goog.inherits(vr.VideoPlaylist, goog.events.EventTarget);


/**
 * @typedef {{
 *   repeat: boolean
 * }}
 */
vr.VideoPlaylist.Settings;


/** @enum {string} */
vr.VideoPlaylist.EventType = {
  CHANGE: 'CHANGE',
};


/**
 * Returns the round that should be playing currently in the playlist.
 * If no round has been played yet, the first in the queue is the current round.
 * @return {vr.Round} The current round to play, or no round to indicate the
 *     end of the queue.
 */
vr.VideoPlaylist.prototype.getCurrentRound = function() {
  return this.rounds_[this.currentRoundIndex_] || null;
};


/**
 * Attempts to move forwards one round. Can move past the end.
 * @return {boolean} Whether or not the round actually changed.
 */
vr.VideoPlaylist.prototype.changeNextRound = function() {
  if (this.currentRoundIndex_ > this.rounds_.length) return false;
  this.currentRoundIndex_++;
  this.dispatchEvent(vr.VideoPlaylist.EventType.CHANGE);
  return true;
};


/**
 * Attempts to move backwards one round.
 * @return {boolean} Whether or not the round actually changed.
 */
vr.VideoPlaylist.prototype.changePreviousRound = function() {
  if (!this.hasPreviousRound()) return false;
  this.currentRoundIndex_--;
  this.dispatchEvent(vr.VideoPlaylist.EventType.CHANGE);
  return true;
};


/** @return {boolean} */
vr.VideoPlaylist.prototype.hasNextRound = function() {
  return this.currentRoundIndex_ < this.rounds_.length;
};


/** @return {boolean} */
vr.VideoPlaylist.prototype.hasPreviousRound = function() {
  return this.currentRoundIndex_ > 0;
};


/**
 * Returns a shallow copy of the list of all rounds.
 * Expected to be used primarily by PlaylistManager.
 * @return {!Array.<!vr.Round>}
 */
vr.VideoPlaylist.prototype.getAllRounds = function() {
  return this.rounds_.slice();
};
