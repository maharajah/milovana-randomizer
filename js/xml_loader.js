goog.provide('vr.XmlLoader');

goog.require('vr.VideoDatabase');
goog.require('vr.VideoSource');



/**
 * @param {string} databasePath Path to the XML database.
 * @constructor
 * @struct
 */
vr.XmlLoader = function(databasePath) {
  /** @private {string} */
  this.databasePath_ = databasePath;

  /** @private {XMLHttpRequest} */
  this.request_ = null;
};


/**
 * Loads the XML database, then creates a structured in-memory database with
 * all the rounds from the expected schema.
 * @param {!function(!vr.VideoDatabase)} callback Callback to invoke with a
 *     constructed database.
 * @param {Object=} opt_context Context in which to invoke the callback.
 */
vr.XmlLoader.prototype.load = function(callback, opt_context) {
  this.request_ = new window.XMLHttpRequest();
  this.request_.open('GET', this.databasePath_);
  this.request_.addEventListener('readystatechange', (function() {
    if (this.request_.readyState == window.XMLHttpRequest.DONE &&
        this.request_.status == 200 &&
        this.request_.responseXML) {
      this.onXmlLoaded_(this.request_.responseXML, callback, opt_context);
    }
  }).bind(this), false);
  this.request_.send();
};


/**
 * Handles parsing the given XML and invoking the callback.
 * @param {!Document} xml
 * @param {!function(!vr.VideoDatabase)} callback
 * @param {Object=} opt_context
 * @private
 */
vr.XmlLoader.prototype.onXmlLoaded_ = function(xml, callback, opt_context) {
  var videos = Array.prototype.map.call(
      xml.getElementsByTagName('VIDEO'), vr.XmlLoader.parseVideoNode_);
  videos = videos.filter(function(video) {
    return !!video.videoIdentifier;
  });
  callback.call(opt_context, new vr.VideoDatabase(videos));
};


/**
 * @param {!Node} videoNode A single VIDEO node.
 * @return {!vr.Video} Video object.
 * @private
 */
vr.XmlLoader.parseVideoNode_ = function(videoNode) {
  var roundNodes = videoNode.getElementsByTagName('Round');

  var video = {
    name: vr.XmlLoader.getText_(videoNode, 'Filename').split('\.')[0],
    rounds: [],
    videoIdentifier: vr.XmlLoader.getText_(videoNode, 'VideoId'),
    videoSource: vr.VideoSource.GOOGLE_DRIVE,
  };

  video.rounds = Array.prototype.map.call(roundNodes, function(roundNode, i) {
    return vr.XmlLoader.parseRoundNode_(roundNode, video, i);
  });

  return video;
};


/**
 * @param {!Node} roundNode A single Round node.
 * @param {!vr.Video} video Containing Video object.
 * @param {number} index Round index in the current video.
 * @return {!vr.Round} Round object.
 * @private
 */
vr.XmlLoader.parseRoundNode_ = function(roundNode, video, index) {
  return {
    endTimeSeconds: vr.XmlLoader.parseTimestamp_(
        vr.XmlLoader.getText_(roundNode, 'EndTime')),
    startTimeSeconds: vr.XmlLoader.parseTimestamp_(
        vr.XmlLoader.getText_(roundNode, 'StartTime')),
    name: 'Round ' + (index + 1),
    video: video,
  };
};


/**
 * @param {string} timestamp Timestamp string, e.g. '50:23' or '01:20:15'.
 * @return {number} Timestamp, in seconds.
 * @private
 */
vr.XmlLoader.parseTimestamp_ = function(timestamp) {
  var times = timestamp.split(':');
  if (times.length == 1) {
    return parseInt(times[0], 10);
  }

  if (times.length == 2) {
    return parseInt(times[0], 10) * 60 + parseInt(times[1], 10);
  }

  if (times.length == 3) {
    return parseInt(times[0], 10) * 3600 + parseInt(times[1], 10) * 60 +
        parseInt(times[2], 10);
  }

  throw new Error('Invalid timestamp');
};


/**
 * Finds the first selector within the given node and returns its text content,
 * if it exists.
 * @param {!Node} node
 * @param {string} selector
 * @return {string} Text content of the selected node.
 * @private
 */
vr.XmlLoader.getText_ = function(node, selector) {
  if (!node) return '';

  var childNode = node.querySelector(selector);
  if (!childNode) return '';
  return childNode.textContent;
};
