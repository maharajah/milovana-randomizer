SHELL := bash
OUTPUT_DIR := bin
JS_SRCS = $(wildcard js/*.js js/*/*.js)

BUILDER = python closure-library/closure/bin/build/closurebuilder.py
COMPILER = closure-compiler/compiler.jar

BUILDER_FLAGS = --root=closure-library --root=js --namespace=vr

all: debug release

debug: JS_DEBUG_OUTPUTS = $(shell $(BUILDER) $(BUILDER_FLAGS))
debug: bin/demo-debug.html

release: JS_OUTPUTS = bin/compiled.js
release: BUILDER_FLAGS += -f "--warning_level=VERBOSE"
release: BUILDER_FLAGS += -f "--js_output_file=bin/compiled.js"
release: BUILDER_FLAGS += -f "--compilation_level=ADVANCED_OPTIMIZATIONS"
release: BUILDER_FLAGS += -f "--generate_exports"
release: BUILDER_FLAGS += -f "--closure_entry_point=vr.VideoRandomizer"

release: BUILDER_FLAGS += -f "--jscomp_error=accessControls"
release: BUILDER_FLAGS += -f "--jscomp_error=ambiguousFunctionDecl"
release: BUILDER_FLAGS += -f "--jscomp_error=checkRegExp"
release: BUILDER_FLAGS += -f "--jscomp_error=checkTypes"
release: BUILDER_FLAGS += -f "--jscomp_error=checkVars"
release: BUILDER_FLAGS += -f "--jscomp_error=const"
release: BUILDER_FLAGS += -f "--jscomp_error=constantProperty"
release: BUILDER_FLAGS += -f "--jscomp_error=deprecated"
release: BUILDER_FLAGS += -f "--jscomp_error=duplicateMessage"
release: BUILDER_FLAGS += -f "--jscomp_error=es5Strict"
release: BUILDER_FLAGS += -f "--jscomp_error=externsValidation"
release: BUILDER_FLAGS += -f "--jscomp_error=fileoverviewTags"
release: BUILDER_FLAGS += -f "--jscomp_error=globalThis"
release: BUILDER_FLAGS += -f "--jscomp_error=invalidCasts"
release: BUILDER_FLAGS += -f "--jscomp_error=missingProperties"
release: BUILDER_FLAGS += -f "--jscomp_error=nonStandardJsDocs"
release: BUILDER_FLAGS += -f "--jscomp_error=strictModuleDepCheck"
release: BUILDER_FLAGS += -f "--jscomp_error=typeInvalidation"
release: BUILDER_FLAGS += -f "--jscomp_error=undefinedNames"
release: BUILDER_FLAGS += -f "--jscomp_error=undefinedVars"
release: BUILDER_FLAGS += -f "--jscomp_error=unknownDefines"
release: BUILDER_FLAGS += -f "--jscomp_error=useOfGoogBase"
release: BUILDER_FLAGS += -f "--jscomp_error=uselessCode"
release: BUILDER_FLAGS += -f "--jscomp_error=visibility"
release: BUILDER_FLAGS += -f "--jscomp_off=checkStructDictInheritance"
release: BUILDER_FLAGS += -f "--jscomp_off=internetExplorerChecks"

release: BUILDER_FLAGS += --output_mode=compiled
release: BUILDER_FLAGS += --compiler_jar=$(COMPILER)
release: bin/compiled.js bin/demo.html

bin/compiled.js: $(JS_SRCS)
	$(BUILDER) $(BUILDER_FLAGS)

bin/demo.html: demo/contents.html
	echo '<script src="compiled.js"></script>' > bin/demo.html
	cat demo/contents.html >> bin/demo.html

bin/demo-debug.html: bin/scripts-debug demo/contents.html
	cat bin/scripts-debug demo/contents.html > bin/demo-debug.html

bin/scripts-debug:
	perl -pe "s/([^\s]+)/<script src='..\/\$$1'><\/script>\n/g" \
        <<< "$(JS_DEBUG_OUTPUTS)" \
		> bin/scripts-debug

clean:
	rm -f bin/*

